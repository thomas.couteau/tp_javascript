//COUTEAU Thomas
// 03/05/2021

//Déclaration de vecteur / tableau
let listeArticles = [['jusOrange', 1.35], ['yaourt', 1.6], ['pain', 0.9], ['jambon', 2.75], ['salade', 0.8], ['spaghettis', 0.95]]; //liste des articles
let nbArticles; //nombre d'articles
let montant; //montant totale des courses
let k1 = 2;//nb articles pour le jus d'orange 
let k2 = 1;//nb articles pour le yaourt nature
let k3 = 1;//nb articles pour le pain de mie
let k4 = 1;//nb articles pour jambon
let k5 = 1;//nb articles pour la salade laitue
let k6 = 2;//nb articles pour les spaghettis

//calcul des prix de chaque articles
let jus = k1 * listeArticles[0][1]; //calcul total jus orange
let yaourt = k2 * listeArticles[1][1]; //calcul total yaourt
let pain = k3 * listeArticles[2][1]; //calcul total du pain de mie
let jambon = k4 * listeArticles[3][1]; //calcul total de la barquette de jambon
let salade = k5 * listeArticles[4][1]; //calcul total de la salade laitue
let spaghettis = k6 * listeArticles[5][1]; //calcul total des spaghettis

//calcul du nombre des articles
nbArticles = k1 + k2 + k3 + k4 + k5 + k6;

//calcul du montant des courses
montant = jus + yaourt + pain + jambon + salade + spaghettis ; 
montant = montant.toFixed(2);

//Affichage console
console.log("Jus d'orange 1L") ;
console.log("  "+ k1 + " X " + listeArticles[0][1] + " EUR " + "   " + jus + " EUR "); 
console.log("Yaourt nature 4X");
console.log("  "+ k2 + " X " + listeArticles[1][1] + " EUR " + "   " + yaourt + " EUR "); 
console.log("Pain de mie 500g");
console.log("  "+ k3 + " X " + listeArticles[2][1] + " EUR " + "   " + pain + " EUR "); 
console.log("Barquette Jambon blanc 4xT");
console.log("  "+ k4 + " X " + listeArticles[3][1] + " EUR " + "   " + jambon + " EUR "); 
console.log("Salade Laitue");
console.log("  "+ k5 + " X " + listeArticles[4][1] + " EUR " + "   " + salade + " EUR "); 
console.log("Spaghettis 500g");
console.log("  "+ k6 + " X " + listeArticles[5][1] + " EUR " + "   " + spaghettis + " EUR "); 
console.log("\nNombre d'articles achetés : " + nbArticles);
console.log("\nMONTANT : " + montant + " EUR");