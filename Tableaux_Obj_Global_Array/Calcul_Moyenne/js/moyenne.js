//COUTEAU Thomas
// 29/04/2021

//Déclaration de vecteur / tableau
const CONST = 6 ; 
let listeValeurs = [CONST];      //[10, 25, 41, 5, 9, 11];
listeValeurs = [10, 25, 41, 5, 9, 11] ; 
let somme ; //variable pour le résultat de la somme
let moyenne ; //variable pour le résultat de la moyenne

//calcul de la somme
somme = listeValeurs[0] + listeValeurs[1] + listeValeurs[2] + listeValeurs[3] + listeValeurs[4] + listeValeurs[5] ; 
//calcul de la moyenne
moyenne = (listeValeurs[0] + listeValeurs[1] + listeValeurs[2] + listeValeurs[3] + listeValeurs[4] + listeValeurs[5]) / CONST ; 

//Affichage console
console.log("Liste des valeurs :");
for(let valeurs of listeValeurs) {
    console.log(valeurs);
}
console.log("Somme des valeurs : " + somme); //affichage console de la somme des valeurs
console.log("Moyenne des valeurs : " + moyenne.toFixed(2)); //affichage console de la moyenne des valeurs