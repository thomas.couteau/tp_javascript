//Auteur : COUTEAU Thomas
//Date : 08/04/2021

//Déclaration de variables
let vitesse = 90; //vitesse en km/h
let distance = 500;  //distance à parcourir en km
let tpsTrajet; //valeur du tpsTrajet


function calculerTempsParcoursSec(prmVitesse, prmDistance) {
    let temps;  // temps de trajet en secondes
    temps = ((prmDistance / prmVitesse) * 3600); //formule pour calculer le temps en fonction de la distance et de la vitesse
    return temps; //valeur de retour du temps
}

tpsTrajet = calculerTempsParcoursSec(vitesse, distance); //utilisation de la fonction pour calculer le temps de trajet

function convertir_h_min_sec(prmTemps) {
    let heures; //variable pour les heures
    let minutes;//variable pour les minutes
    let aff;//variable l'affichage

    heures = Math.floor(prmTemps / 3600);
    prmTemps %= 3600;
    minutes = Math.floor(prmTemps / 60);
    prmTemps %= 60;

    aff = heures + "h    " + minutes + "min " + prmTemps + "s";

    return aff;
}
//affichage console
console.log("Calcul du temps de parcours d'un trajet :"); //affichage console pour la présentation 
console.log("\t Vitesse moyenne (en km/h) : " + vitesse); //affichage console pour la vitesse
console.log("\t Distance à parcourir (en km) : " + distance);//affichage console pour la distance

//affichage console de la phrase de fin avec l'utilisation de la fonction pour la convertion du temps
console.log("A " + vitesse + "km/h, une distance de " + distance + " km  est parcourue en " + convertir_h_min_sec(tpsTrajet));
