//Auteur : COUTEAU Thomas
//Date : 26/04/2021


// Déclarations de variables
let taille = 175; //taille en cm
let poids = 100; //poids en kg
let IMC; //variable de retour pour l'IMC
let interpretation_msg; //variable pour afficher l'intérpretation


function decrire_corpulence(prmTaille, prmPoids) {  // fonctions imbriquée
    let valIMC; // IMC
    let interpretation = "";    // Interprétation de l'IMC

    function calculerIMC(prmTaille, prmPoids) {
        valIMC = poids / ((taille * taille) * 10e-5);   //Calcul de l'IMC
        valIMC = valIMC.toFixed(1);
    }

    function interpreterIMC() {
        // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
        if (valIMC < 16.5) {
            interpretation = "Dénutrition"
        }
        else if ((valIMC > 16.5 && valIMC < 18.5)) {
            interpretation = "Maigreur"
        }
        else if ((valIMC > 18.5) && (valIMC < 25)) {
            interpretation = "Corpulence Normale"
        }
        else if ((valIMC > 25) && (valIMC < 30)) {
            interpretation = "Surpoids"
        }
        else if ((valIMC > 30) && (valIMC < 35)) {
            interpretation = "Obésité Modérée"
        }
        else if ((valIMC > 35) && (valIMC < 40)) {
            interpretation = "Obésité Sévère"
        }
        else {
            interpretation = "Obésité Morbide"
        }
    }
    //traitement et utilisation des fonctions
    calculerIMC();
    interpreterIMC();
    //affichage du resultat
    return "Votre IMC est égal à " + valIMC + " : vous êtes en état de " + interpretation
}

//affichage console
console.log("Calcul de l'IMC"); //pour la présentation
console.log("Taille :" + taille + "(en cm)"); //affichage console de la taille
console.log("Poids :" + poids + "(en kg)"); //affichage console du poids

// Affichage de l'interprétation de l'IMC dans la console du navigateur
console.log(decrire_corpulence(taille, poids));