//Auteur : COUTEAU Thomas
//Date : 08/04/2021

//Déclaration de variables
let valeurLimite = 20; //valeur limite de la recherche

//fonction de la recherche des multiples de 3
function rechercher_Mult3(prmLimite) {
    let multiples; //variable pour rechercher les multiples 
    let aff = ""; //variable d'affichage

    for (multiples = 0; multiples < prmLimite; multiples++) {

        if (multiples % 3 == 0) {
            aff = aff + " " + multiples; 
        }

    }
    return aff; //retourne l'affichage
}
//affichage console
console.log("Recherche des multiples de 3 :"); //affichage console pour la présentation
console.log("Valeur limite de la recherche : " + valeurLimite);//affichage console la valeur limite à rechercher
console.log("Multiples de 3 : " + rechercher_Mult3(valeurLimite));//afichage console des multiples de 3
