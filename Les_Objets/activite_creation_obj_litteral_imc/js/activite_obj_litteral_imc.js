// COUTEAU Thomas
// 27/04/2021

//Déclaration de variables et d'objets
let objPatient = {    // Création d'un objet patient
    nom: 'Dupond', //nom du patient
    prenom: 'Jean', //prenom du patient
    age: 30, //age du patient
    sexe: 'masculin',// sexe du patient
    taille: 180, //taille du patient en cm
    poids: 85, //poids du patient en kg
    valIMC : undefined,
    decrire: function () { //fonction qui va décrire le patient
        let description;
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        return description; //on retourne la description
    },
    calculer_IMC: function () {
        valIMC = this.poids / ((this.taille * this.taille) * 10e-5);   //Calcul de l'IMC
        this.valIMC = valIMC.toFixed(2);
        return this.valIMC; //on retourne la valeur de l'IMC
    },
    interpreter_IMC: function () {
        // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
        if (this.valIMC < 16.5) {
            interpretation = "Dénutrition";
        }
        else if ((this.valIMC > 16.5 && this.valIMC < 18.5)) {
            interpretation = "Maigreur";
        }
        else if ((this.valIMC > 18.5) && (this.valIMC < 25)) {
            interpretation = "Corpulence Normale";
        }
        else if ((this.valIMC > 25) && (this.valIMC < 30)) {
            interpretation = "Surpoids";
        }
        else if ((this.valIMC > 30) && (this.valIMC < 35)) {
            interpretation = "Obésité Modérée";
        }
        else if ((this.valIMC > 35) && (this.valIMC < 40)) {
            interpretation = "Obésité Sévère";
        }
        else {
            interpretation = "Obésité Morbide";
        }
        return interpretation;
    }
};

//Affichage console de la description, de la valeur de l'IMC et de l'état de corpulence du patient
console.log(objPatient.decrire()); //affichage console de la description
console.log("Son IMC est de :" + objPatient.calculer_IMC()); //affichage console du calcul d'IMC
console.log("Il est en situation de " + objPatient.interpreter_IMC()); //affichage console de l'état du patient
