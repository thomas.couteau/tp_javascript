// COUTEAU Thomas
// 27/04/2021

//Déclaration de variables et d'objets
let objPatient = {    // Création d'un objet patient
    nom: 'Dupond', //nom du patient
    prenom: 'Jean', //prenom du patient
    age: 30, //age du patient
    sexe: 'masculin',// sexe du patient
    taille: 180, //taille du patient en cm
    poids: 85, //poids du patient en kg
    decrire: function () { //fonction qui va décrire le patient
        let description;
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        return description; //on retourne la description
    },
    definir_corpulence: function () {
        let interpretation;
        let valIMC = undefined;
        let poids = this.poids;
        let taille = this.taille;
        function calculer_IMC() {
            valIMC = poids / ((taille * taille) * 10e-5); //Calcul de l'IMC
            valIMC = valIMC.toFixed(2);
        }
        function interpreter_IMC() {
            // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
            if (valIMC < 16.5) {
                interpretation = "Dénutrition";
            }
            else if ((valIMC > 16.5 && valIMC < 18.5)) {
                interpretation = "Maigreur";
            }
            else if ((valIMC > 18.5) && (valIMC < 25)) {
                interpretation = "Corpulence Normale";
            }
            else if ((valIMC > 25) && (valIMC < 30)) {
                interpretation = "Surpoids";
            }
            else if ((valIMC > 30) && (valIMC < 35)) {
                interpretation = "Obésité Modérée";
            }
            else if ((valIMC > 35) && (valIMC < 40)) {
                interpretation = "Obésité Sévère";
            }
            else {
                interpretation = "Obésité Morbide";
            }
        }
        calculer_IMC();
        interpreter_IMC();
        return "Son IMC est de : " + valIMC + "\nIl est en situation de " + interpretation;
    },
};

//Affichage console de la description, de la valeur de l'IMC et de l'état de corpulence du patient
console.log(objPatient.decrire()); //affichage console de la description
console.log(objPatient.definir_corpulence()); //affichage console de la définition de la corpulence du patient