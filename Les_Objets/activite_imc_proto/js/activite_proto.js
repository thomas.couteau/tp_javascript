//COUTEAU Thomas
// 28/04/2021

//Déclarations de variables
function Patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {    // Constructeur d'un patient 
    this.nom = prmNom; // nom du patient
    this.prenom = prmPrenom; // prenom du patient
    this.age = prmAge;    // age du patient 
    this.sexe = prmSexe;  // sexe du patient 
    this.taille = prmTaille;    // taille du patient 
    this.poids = prmPoids; // poids du patient 
    Patient.prototype.decrire = function () {  // fonction de description du patient
        let description;
        let sexe = this.sexe;
        if (sexe == 'masculin') {
            description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        }
        else if (sexe == 'féminin') {
            description = "La patiente " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agée de " + this.age + " ans. Elle mesure " + this.taille * 0.01 + "m et pèse " + this.poids + "kg";
        }
        return description;
    };
    Patient.prototype.definir_corpulence = function () {
        let interpretation;
        let valIMC = undefined;
        let poids = this.poids;
        let taille = this.taille;
        let sexe = this.sexe;
        function calculer_IMC() {
            valIMC = poids / ((taille * taille) * 10e-5); //Calcul de l'IMC
            valIMC = valIMC.toFixed(2);
        }
        function interpreter_IMC() {
            // On choisit le critère de l'IMC en mettant un si avec comme condition l'IMC
            if (sexe == 'masculin') {   //si le patient est un homme  
                if (valIMC < 16.5) {
                    interpretation = "Dénutrition";
                }
                else if ((valIMC > 16.5 && valIMC < 18.5)) {
                    interpretation = "Maigreur";
                }
                else if ((valIMC > 18.5) && (valIMC < 27)) {
                    interpretation = "Corpulence Normale";
                }
                else if ((valIMC > 27) && (valIMC < 30)) {
                    interpretation = "Surpoids";
                }
                else if ((valIMC > 30) && (valIMC < 35)) {
                    interpretation = "Obésité Modérée";
                }
                else if ((valIMC > 35) && (valIMC < 40)) {
                    interpretation = "Obésité Sévère";
                }
                else {
                    interpretation = "Obésité Morbide";
                }
            }
            else if (sexe == 'féminin') {   //si le patient est une femme
                if (valIMC < 16.5) {
                    interpretation = "Dénutrition";
                }
                else if ((valIMC > 16.5 && valIMC < 18.5)) {
                    interpretation = "Maigreur";
                }
                else if ((valIMC > 18.5) && (valIMC < 25)) {
                    interpretation = "Corpulence Normale";
                }
                else if ((valIMC > 25) && (valIMC < 30)) {
                    interpretation = "Surpoids";
                }
                else if ((valIMC > 30) && (valIMC < 35)) {
                    interpretation = "Obésité Modérée";
                }
                else if ((valIMC > 35) && (valIMC < 40)) {
                    interpretation = "Obésité Sévère";
                }
                else {
                    interpretation = "Obésité Morbide";
                }
            }
            else {
                interpretation = "Erreur";
            }
        }
        calculer_IMC();
        interpreter_IMC();
        return "Son IMC est de : " + valIMC + "\nIl est en situation de " + interpretation;
    };
}
//création des objets Patient 
let objPatient1 = new Patient('Dupond', 'Jean', 30, 'masculin', 180, 85);
let objPatient2 = new Patient('Moulin', 'Isabelle', 46, 'féminin', 158, 74);
let objPatient3 = new Patient('Martin', 'Eric', 42, 'masculin', 165, 90);
//Affichage console des patients
console.log(objPatient1);
console.log(objPatient2);
console.log(objPatient3);