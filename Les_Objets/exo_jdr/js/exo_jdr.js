//COUTEAU Thomas
// 28/04/2021

//fonction personnage
function Personnage(prmNom, prmNiveau) {
    this.nom = prmNom; //nom du personnage
    this.niveau = prmNiveau; //niveau du personnage
}
//fonction saluer
Personnage.prototype.saluer = function () {
    let description; //description du personnage
    description = this.nom + " vous salue !!";
    return description; //retour de la description
}
//fonction guerrier
function Guerrier(prmNom, prmNiveau, prmArme) {
    Personnage.call(this, prmNom, prmNiveau);
    this.arme = prmArme; //arme du guerrier
}
Guerrier.prototype = Object.create(Personnage.prototype); //heritage vers le constructeur Personnage
Guerrier.prototype.constructor = Guerrier;

//fonction combattre
Guerrier.prototype.combattre = function () {
    let description; //description de la fct combattre
    description = this.nom + " est un guerrier qui se bat avec " + this.arme;
    return description; //retour de la description de la fct combattre
}
//création d'un guerrier
let objGuerrier = new Guerrier('Arthur', 3, 'une épée');

//Affichage console du personnage guerrier
console.log(objGuerrier.saluer());
console.log(objGuerrier.combattre());

//fonction magicien
function Magicien(prmNom, prmNiveau, prmPouvoir) {
    Personnage.call(this, prmNom, prmNiveau);
    this.pouvoir = prmPouvoir; //pouvoir du magicien
}
Magicien.prototype = Object.create(Personnage.prototype); //heritage vers le constructeur Personnage
Magicien.prototype.constructor = Magicien;

//fonction posseder
Magicien.prototype.posseder = function () {
    let description; //description de la fct posseder
    description = this.nom + " est un magicien qui possède le pouvoir de " + this.pouvoir;
    return description; //retour de la description de la fct posseder
}
//création d'un magicien
let objMagicien = new Magicien('Merlin', 2, 'prédire les batailles');

//Affichage console du personnage magicien
console.log(objMagicien.saluer());
console.log(objMagicien.posseder());