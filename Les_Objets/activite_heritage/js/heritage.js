//COUTEAU Thomas
// 28/04/2021

//Déclaration de fonction Personne
function Personne(prmNom, prmPrenom, prmAge, prmSexe) {
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
}

Personne.prototype.decrire = function () {   //Description de la personne 
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans";
    return description;
}
//Constructeur professeur

function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {
    Personne.call(this, prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}

Professeur.prototype = Object.create(Personne.prototype);   //Heritage du constructeur Personne()
Professeur.prototype.constructor = Professeur;  // réinitialise la propriété constructeur Professeur

// fonction decrire_plus() pour Professeur()
Professeur.prototype.decrire_plus = function () {
    let description;
    let prefixe;
    if (this.sexe == 'M') { //si le prof est un homme
        prefixe = 'Mr';
    } else {
        prefixe = 'Mme';  //si le prof est une femme
    }
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

// Constructeur Eleve 
function Eleve(prmNom, prmPrenom, prmAge, prmSexe) {
    Personne.call(this, prmNom, prmPrenom, prmAge, prmSexe);
}

Eleve.prototype = Object.create(Personne.prototype);   // Heritage du constructeur Personne()
Eleve.prototype.constructor = Eleve;  // réinitialise la propriété constructor Eleve

Eleve.prototype.decrire_plus = function () {    // méthode decrire_plus() pour Eleve
    let description;
    let prefixe;
    if (this.sexe == 'M') {
        prefixe = 'un';
    } else {
        prefixe = 'une';
    }
    description = this.prenom + " " + this.nom + " est " + prefixe + " élève de SNIR1";
    return description;
}

// Affichage console du professeur
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());

// Affichage console de l'élève 
let objEleve1 = new Eleve('Dutillieul', 'Dorian', 18, 'M');
console.log(objEleve1.decrire());
console.log(objEleve1.decrire_plus());