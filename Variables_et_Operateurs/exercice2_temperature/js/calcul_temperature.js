//déclaration de variables
let celcius = 22.6 ; //valeur de la température en °C

//conversion du degré °C en °F
let far = (celcius*1.8) + 32 ; 

//affichage
let aff = "Une température de " + celcius + "°C correspond à une température de " + far.toFixed(1) + "°F.";
console.log(aff);