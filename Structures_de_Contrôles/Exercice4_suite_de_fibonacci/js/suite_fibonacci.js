//Auteur : COUTEAU Thomas 
//Date 08/04/2021

//Déclaration de variables
let nb = 0; //premier nb de la suite
let nb2 = 1; //deuxième nb de la suite
let valMax = 17; //valeur maximale
let resultat; //resultat du calcul de nb + nb2
let i ; //index de "déplacement"
let aff = "Suite de Fibonacci : " + nb + " " + nb2; //variable pour l'affichage 


for (i = 0; i < valMax ; i++) {

  resultat = nb + nb2; //Calcul pour la suite de Fibonacci
  nb = nb2; 
  nb2 = resultat; //nb2 prend la valeur de resultat

  aff = aff + " " + resultat; //actualisation de l'affichage
}

//affichage
console.log(aff); //affichage console