//Auteur : COUTEAU Thomas
//Date : 08/04/2021

// Déclarations de variables
let nbDepart = 2;  //nombre de départ pour l'exercice
let i;  //index de déplacement
let aff = "";  //affichage pour la suite des 12 nombres dont chaque terme soit égal au triple du terme précédent

//affichage
console.log("Valeur de départ : " + nbDepart);//affichage console de la valeur de départ

//Pour
for (i = 2; i < 13; i++) {

    nbDepart = nbDepart * 3; //multiplication par 3 de la valeur de débart
    aff = aff + nbDepart + " ";
}
//affichage
console.log("Valeurs de la suite : " + aff);//affichage console de la suite
