//Auteur : COUTEAU Thomas
//Date : 08/04/2021

//Déclaration de variables
const k = 7; //constante du multiplicateur 7
let resultat = 0; //résultat de la table
let compteur = 0 ;  //compteur pour  la boucle de calcul de la table de 7
let aff = "";  //affichage de la table

for (compteur = 1; compteur < 20; compteur++) {    //boucle pour calculer les mutilples de 7

    resultat = k * compteur ;
    aff = aff + " " + resultat ;

    if (compteur % 3 == 0) { //pour afficher une * lorsque le nombre est multiple de 3 

        aff = aff + " *"    //affiche une * pour les multiples de 3
    }
}
//affichage 
console.log(aff);//affichage console de la table de multiplication par 7