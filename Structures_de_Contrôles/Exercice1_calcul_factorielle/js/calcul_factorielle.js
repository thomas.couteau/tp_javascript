//Déclaration de variables
let n; //nb de facteur 
let compteur; //compteur pour la boucle 
let valMax = 10;//valeur maximale pour la factorielle
let valRetour = 0; //valeur de retour de la factorielle
let aff; //affichage

//Traitement

if (valMax > 1) {//si valMax est supérieur à 1 

    console.log("1! = 1"); //afficher la première ligne de la factorielle

    for (n = 2; n < valMax; n++) {//Pour chaque valeur de la factorielle

        aff = n + "! = 1";
        valRetour = 1;

        for (compteur = 2; compteur <= n; compteur++) {

            aff += " x " + compteur; //On ajoute au message
            valRetour = valRetour * compteur;//calcul de la factorielle
        }
        aff += " = " + valRetour; //affichage de la factorielle
        console.log(aff); //affichage console
    }
}