//déclaration de variable
let taille = 175; //taille en cm
let poids = 100; //poids en kg
let aff = "Votre IMC est de = "; //variable de retour
let message = undefined; //message pour l'intérpretation

//traitement
let resultat = (poids / (taille * taille)); //calcul de l'imc
resultat = resultat * 10000;

taille = "Taille = " + taille; //affichage de la taille
poids = "Poids = " + poids;  //affichage du poids
aff = aff + resultat.toFixed(1); //affichage de l'IMC

//affichage console
console.log(taille); //affichage console de la taille
console.log(poids); //affichage console du poids
console.log(aff);//affichage de la valeur

let interpretation = "Interprétation de l'IMC = ";
console.log(interpretation); 
//boucle pour l'interprétation de l'IMC et affichage
if (resultat < 16.5) {
    console.log("Dénutrition");
}
if ((resultat > 16.5) && (resultat < 18.5)) {
    console.log("Maigreur");
}
if ((resultat > 18.5) && (resultat < 25)) {
    console.log("Corpulence normale");
}
if ((resultat > 25) && (resultat < 30)) {
    console.log("Surpoids");
}
if ((resultat > 30) && (resultat < 35)) {
    console.log("Obesite modérée");
}
if ((resultat > 35) && (resultat < 40)) {
    console.log("Obesite severe'");
}
if (resultat > 40) {
    console.log("Obesite morbide");
}
  