//Déclaration de variables

let valMax = 16384; //valeur en euro à convertir en dollar
convEuroDollar(valMax);

//Déclaration de fonction
function convEuroDollar(prmValMax) {
    //Affiche les convertions euros/dollar de valMax

    const TAUX = 1.65; //constante = taux de change 1€ = 1.65$
    let valeurEuro = 1; //valeur de l'euro 
    let valeurDollar = valeurEuro * TAUX; //convertion de l'euro en dollar
    let affichage = ""; //affichage des convertions calculées
    //Boucle d'affichage
    while (valeurEuro <= prmValMax) {
        
        //pour afficher les convertions à la suite
        affichage = affichage + valeurEuro + " euro(s) = " + valeurDollar.toFixed(2) + " dollar(s)" + "\n";
        valeurEuro = valeurEuro * 2; 
        valeurDollar = valeurEuro * TAUX; 
    }
    //affichage console des conversions
    console.log(affichage);
}