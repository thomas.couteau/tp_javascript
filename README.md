# tp_javascript

Mes Tp JavaScript

> * Auteur : Thomas Couteau
> * Date de publication : 06/04/2021
> * OS : Windows 10
> * EDI : VS Code
> * Navigateur Web : Chrome
## Sommaire
- 1. Introduction
  - [HelloWorld](Introduction/HelloWorld_JS/index.html)
- 2. Variables et Opérateurs en JavaScript
  - [Activité : Calcul de surface](Variables_et_Operateurs/activité_surface/index.html)
  - [Exercice 1 : Calcul d'IMC](Variables_et_Operateurs/exercice1_calcul_imc/index.html)
  - [Exercice 2 : Convertion d'une température](Variables_et_Operateurs/exercice2_temperature/index.html)
- 3. Les structures de contrôle en JavaScript
  - [Activité : Calcul d'IMC et Interprétation](Structures_de_Contrôles/activité_interpretation_imc/index.html)
  - [Activité : Conjecture de Syracuse](Structures_de_Contrôles/activité_conjecture_syracuse/index.html)
  - [Exercice 1 : Calcul de factorielle](Structures_de_Contrôles/Exercice1_calcul_factorielle/index.html)
  - [Exercice 2 : Convertion Euro/Dollar](Structures_de_Contrôles/Exercice2_convertion_euro_dollar/index.html)
  - [Exercice 3 : Nombres triples](Structures_de_Contrôles/Exercice3_nombres_triples/index.html)
  - [Exercice 4 : Suite de Fibonacci](Structures_de_Contrôles/Exercice4_suite_de_fibonacci/index.html)
  - [Exercice 5: Table de multiplication](Structures_de_Contrôles/Exercice5_table_de_multi/index.html)
- 4. Les fonctions
  - [Activité: Codage de fonctions pour calculer et interpréter l'IMC](Les_Fonctions/activité_fct_imc/index.html)
  - [Exercice 1: calcul du temps de parcours d'un trajet](Les_Fonctions/Exercice1_calcul_tps_parcours/index.html)
  - [Exercice 2: recherche du nombre de multiples de 3](Les_Fonctions/Exercice2_multiples_de_3/index.html)
  - [Exercice fonction imbriquée](Les_Fonctions/Exercice_fonction_imbriquée/index.html)
- 5. Les objets 
  - [Activité: Création d'un objet littéral pour le calcul de l'IMC V1](Les_Objets/activite_creation_obj_litteral_imc/index.html)
  - [Activité: Création d'un objet littéral pour le calcul de l'IMC V2](Les_Objets/activite_obj_litteral_v2/index.html)
  - [Activité: Codage d'un constructeur d'objet pour le calcul de l'IMC V1](Les_Objets/constructeur_v1/index.html)
  - [Activité: Codage d'un constructeur d'objet pour le calcul de l'IMC V2](Les_Objets/constructeur_v2/index.html)
  - [Activité: Optimisation du codage du constructeur pour le calcul de l'IMC](Les_Objets/activite_imc_proto/index.html)
  - [Activité: implémentation de l'héritage pour gérer les professeurs et les élèves](Les_Objets/activite_heritage/index.html)
  - [Exercice Jeu de Rôle](Les_Objets/exo_jdr/index.html)
- 6. Les tableaux en JavaScript et l'objet global Array
  - [Activité: Calcul de la moyenne d'une série de valeurs](Tableaux_Obj_Global_Array/Calcul_Moyenne/index.html)
  - [Activité : Gestion d'une liste d'articles](Tableaux_Obj_Global_Array/Liste_Articles/index.html)
  - [Activité : Utilisation d'un tableau pour gérer une liste de patients au niveau du calcul de l'IMC](Tableaux_Obj_Global_Array/Tableau_Patient_IMC/index.html)